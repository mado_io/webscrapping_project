import requests
import os

#On récupère le token de connexion, stocké en variable d'environnement
bearer_token = os.environ['BEARER_TOKEN']
print(bearer_token)

dates=[
    "2010-01-01",
    "2011-01-01",
    "2012-01-01",
    "2013-01-01",
    "2014-01-01",
    "2015-01-01",
    "2016-01-01",
    "2017-01-01",
    "2018-01-01",
    "2019-01-01",
    "2020-01-01",
    "2021-01-01"
]


def create_url(date):
    url = "https://api.twitter.com/2/tweets/search/all?query=from:barackObama&start_time="+date+"T00:00:01Z&end_time="+date+"T23:00:01Z&tweet.fields=created_at&expansions=author_id&user.fields=created_at&max_results=1"
    return url


def getobamaid():
    url = "https://api.twitter.com/2/users/by/username/BarackObama"
    return url


def bearer_oauth(r):
    r.headers["Authorization"] = f"Bearer {bearer_token}"
    r.headers["User-Agent"] = "v2ListTweetsLookupPython"
    return r


def connect_to_endpoint(url):
    response = requests.request(
        "GET", url, auth=bearer_oauth)
    print(response.status_code)
    if response.status_code != 200:
        raise Exception(
            "Request returned an error: {} {}".format(
                response.status_code, response.text
            )
        )
    return response.json()


def main():
    print("Getting obama's user id")
    url = getobamaid()
    json_response = connect_to_endpoint(url)
    id_obama = json_response['data']['id']
    print("The id is: "+id_obama)
    tweets_info=[]
    for date in dates:
        print("Getting tweets for the "+date)
        url_search = create_url(date)
        actual_tweet = connect_to_endpoint(url_search)
        tweets_info.append(actual_tweet)
    print(tweets_info)

main()
